
<!-- PROJECT LOGO -->

  <h3 align="center">Pet Clinic Appointment</h3>

  <p align="center">
    A simple pet clinic controller
    <br />
  </p>
</p>

<!-- TABLE OF CONTENTS -->
<details open="open">
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
      <ul>
        <li><a href="#built-with">Built With</a></li>
      </ul>
    </li>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#prerequisites">Prerequisites</a></li>
        <li><a href="#installation">Installation</a></li>
      </ul>
    </li>
    <li><a href="#contact">Contact</a></li>    
  </ol>
</details>

<!-- ABOUT THE PROJECT -->

## About The Project

<p align="center">
  <img src="src/images/PetClinic.gif" alt="Pet Clinic Screenshot" width="400" height="550"/>
</p>
</p>


During the React.js: Building an Interface course, I built this application. Some concepts learned:
- Setting up React with CSS modules
- Navigating the directory structure
- Adding components to an app
- Using CSS syntax with components
- Leveraging other styles
- Using grid components
- Developing interactions and responsiveness
- Implementing media queries
- Combining styles
-  Tailwind CSS

[Course Link](https://www.linkedin.com/learning/react-js-building-an-interface-8551484)

### Built With

This was you built using.

- [React Js](https://reactjs.org/)
- [create-react-app](https://github.com/facebook/create-react-app)
- [Tailwind CSS](https://tailwindcss.com/)
- [React Icons](https://react-icons.github.io/react-icons)

<!-- GETTING STARTED -->

## Getting Started

To get a local copy up and running follow these simple example steps.

### Prerequisites

This is an example of how to list things you need to use the software and how to install them.

- yarn
  ```sh
  npm install --global yarn
  ```

### Installation

1. Clone the repo
   ```sh
   git clone https://gitlab.com/osvaldolpjunior/petclinic.git
   ```
2. Install Yarn packages
   ```sh
   yarn install
   ```
3. Execute using Yarn
   ```
   yarn start
   ```

<!-- CONTACT -->

## Contact

Osvaldo Junior - osvaldo.junior@and.digital

Project Link: [https://gitlab.com/osvaldolpjunior/petclinic](https://gitlab.com/osvaldolpjunior/petclinic)

<!-- MARKDOWN LINKS & IMAGES -->
<!-- https://www.markdownguide.org/basic-syntax/#reference-style-links -->
